# Gestión de usuarios

# API (Backend)
```
Laravel: ^8.12
php: ^7.3 | ^8.0,
```

## Instalar dependencias
Una vez descargado el proyecto del repositorio se debe acceder al directorio llamado api/ donde se encuentra el API desarrollado con el framework Laravel de PHP. Ubicado en la carpeta se deben instalar las dependencias para ello se ejecuta el siguiente comando:
```
composer install
```

## Base de datos
Como motor de base de datos se implemento:
```
MySQL junto con phpmyadmin
```
Para configurar la base de datos del proyecto se debe crear un archivo llamado ".env" (lleva un punto al principio del nombre) en la raiz del directorio api/, para esto tomar como guìa el archivo:
```
.env.example
```
En el phpmyadmin se debe crear una base de datos con el nombre "codesa". IMPORTANTE: Este es el nombre de la base de datos que se debe agregar en el archivo ".env" recientemente creado. Este es un ejemplo de configuración del archivo ".env" para hacer la conexión a la base de datos (modificar solo las siguientes líneas):
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=codesa
DB_USERNAME=root
DB_PASSWORD=
```
Ejecutar el siguiente comando:
```
php artisan key:generate
```
Para cargar algunos datos ('ADMINISTRADOR', 'AUDITOR', 'AUXILIAR') a la tabla "ROL" ejecutar el siguiente comando:
```
php artisan migrate:refresh --seed
```

## Iniciar servidor
Para iniciar el servidor y poder acceder a la base de datos ejecutar el siguiente comando:
```
php artisan serve
```
El API debe ser consumida a la siguinete url:
```
http://127.0.0.1:8000/api
```

# Aplicación (Frotend)
```
Angular CLI: 10.2.1
Node: 15.0.1
```
## Instalar dependencias
Una vez descargado el proyecto del repositorio se debe acceder al directorio llamado app/ donde se encuentra el frontend desarrollado con angular. Ubicado en la carpeta se deben instalar las dependencias para ello se ejecuta el siguiente comando:
```
npm install
```
## Iniciar servidor
Para iniciar el servidor ejecutar el siguiente comando:
```
ng serve ó npm start
```
En el navegador de preferencia ingresar el enlace para ver el proyecto:
```
http://localhost:4200/
```

## Nota:
Es importante tener en cuenta que la url para consumir el API es la siguiente (en esa url corre el API desarrollado con Laravel):
```
http://127.0.0.1:8000/api
```
Como se puede observar en el archivo environment.ts que se encuentra en la siguiente ubicación:
```
aguitacoco/src/environments/environment.ts

```

# Tecnologías:
## Implementadas en el API:
```
1. Laravel (PHP)
2. MySQL (Motor de base de datos)
```
## Implementadas en la aplicación:
```
1. Angular 10 (JS)
2. Angular Material (estilos)
```

## Implementadas en el control de versiones:
```
1. Git
2. Bitbucket (repositorio)
```

# Instrucciones de la Aplicación:
Dentro de ella se pueden realizar las siguientes acciones:
```
1. Ver el dashboard donde están visibles los diferentes usuarios en una tabla.
2. Ver el detalle de cada uno de los usuarios al darle click sobre la opción "seleccionar" de su respectiva fila.
3. Filtrar usuarios, al ingresar su nombre en el primer campo llamado "Nombre"
4. Crear usuarios.
5. Editar usuarios.
6. Eliminar usuarios.
```
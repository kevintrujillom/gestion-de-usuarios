<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Rol;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderByDesc('id')->get();
        
        if ($users) {

            return response()->json([ 
                'success' => true,
                'response' => $users
            ], 200);

        }

        return response()->json([ 
            'success' => false,
            'response' => 'No hay resultados'
        ], 404);     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $activo = $request->input('activo');

        if ($request->input('nombre') && $request->input('rol') &&
            is_numeric($activo)) {

            return $this->saveOrUpdate($request);
    
        } else {
    
            return response()->json([ 
                'success' => false,
                'response' => "Algunos campos están vacios."
            ], 400); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->saveOrUpdate($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if ($user) {

            $user->delete();

            return response()->json([ 
                'success' => true,
                'response' => "Usuario {$user->nombre}, eliminado exitosamente."
            ], 200);

        }

        return response()->json([ 
            'success' => false,
            'response' => "Usuario con id: {$id}, no encontrado."
        ], 404); 
    }

    private function saveOrUpdate(Request $request, $id = null)
    {    
        $user = User::find($id);

        if ($user) {

            $user->update([
                'nombre' => $request->input('nombre'),
                'rol_id' => intval($request->input('rol')),
                'activo' => $request->input('activo'),
            ]);

            return response()->json([ 
                'success' => true,
                'response' => "Usuario {$user->nombre}, actualizado exitosamente."
            ], 200);
        
        } else {

            $user = User::create([
                'nombre' => $request->input('nombre'),
                'rol_id' => intval($request->input('rol')),
                'activo' => $request->input('activo'),
            ]);
            
            if ($user) {
    
                return response()->json([ 
                    'success' => true,
                    'response' => "Usuario {$user->nombre}, guardado exitosamente."
                ], 200);
                
            } else {
    
                return response()->json([ 
                    'success' => false,
                    'response' => "No se pudo guardar el usuario."
                ], 400);              
            }            

        }

    }

        /**
     * Search users by name
     *
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    public function getUsersByName($name)
    {
        $users_array = [];
        
        if ($name == 'all') {
        
            $users = User::orderByDesc('id')->get();
        
        } else {
            
            $users = User::where('nombre', 'like', '%'.$name.'%')->orderByDesc('id')->get();
        }

        if (count($users) > 0) {

            foreach($users as $user){
                $users_array[] = [
                    'id' => $user->id,
                    'nombre' => $user->nombre,
                    'activo' => $user->activo,
                    'rol' => $user->rol,
                ];
            }

            return response()->json([ 
                'success' => true,
                'response' => $users_array
            ], 200);
        }

        return response()->json([ 
            'success' => false,
            'response' => 'No hay resultados.'
        ], 200);  
    }

}

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UsersController;
use App\Http\Controllers\RolsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/listar-usuarios', [UsersController::class, 'index']);
Route::get('/listar-usuario/{id}', [UsersController::class, 'show']);
Route::post('/crear-usuario', [UsersController::class, 'store']);
Route::put('/actualizar-usuario/{id}', [UsersController::class, 'update']);
Route::delete('/eliminar-usuario/{id}', [UsersController::class, 'destroy']);
Route::get('/buscar-por-nombre/{nombre}', [UsersController::class, 'getUsersByName']);

Route::get('/listar-roles', [RolsController::class, 'index']);

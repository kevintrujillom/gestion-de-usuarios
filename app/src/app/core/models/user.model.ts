export class User {
    id: number;
    nombre: string;
    activo: string;
    rol: {
        id: number,
        nombre: string,
        created_at: Date,
        updated_at: Date
    };
}
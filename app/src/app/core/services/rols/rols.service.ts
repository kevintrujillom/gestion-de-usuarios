import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from './../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RolsService {

  constructor( private http: HttpClient ) { }
  
  getRols(): any {
    return this.http.get(`${environment.url_api}/listar-roles/`);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from './../../../../environments/environment';

@Injectable({
  providedIn: 'root'
}) 
export class UsersService {

  constructor( private http: HttpClient ) { }

  httpOptions(): any {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    };

    return httpOptions;
  }

  getAll(): any {
    return this.http.get(`${environment.url_api}/listar-usuarios/`)
  }

  getUser(id: number): any {
    return this.http.get(`${environment.url_api}/listar-usuario/${id}`)
  }

  createUser(user): any {
    return this.http.post(`${environment.url_api}/crear-usuario/`, user)
  }

  updateUser(id: number, changes): any {
    return this.http.put(`${environment.url_api}/actualizar-usuario/${id}`, changes)
  }  

  deleteUser(id: number): any {
    return this.http.delete(`${environment.url_api}/eliminar-usuario/${id}`)
  }

  getUsersByName(name: string): any {
    return this.http.get(`${environment.url_api}/buscar-por-nombre/${name}`);
  }

}

import { Component, OnInit, } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

import { User } from './../core/models/user.model';
import { UsersService } from './../core/services/users/users.service';

import { Rol } from './../core/models/rol.model';
import { RolsService } from './../core/services/rols/rols.service';

import { DialogDeleteComponent } from './../shared/components/dialog-delete/dialog-delete.component';
import { exitCode } from 'process';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  rols: Rol[] = Array();
  users: User[] = Array();
  nameUser = '';
  nameTemporal = '';
  actualPage: number = 1;

  tableInfoUser = false;
  saveActive = false;
  updateActive = false;
  editActive = false;
  deleteActive = false;
  editFieldsActive = false;
  selectedValue = 0;
  userExist = false;
  spinner = false;

  formSearch: FormGroup;
  formCreate: FormGroup;

  constructor(
    private usersService: UsersService, 
    private rolsService: RolsService,
    public dialog: MatDialog,
    private toastr: ToastrService
  ){
    this.formSearch = this.searchFormGroup();
    this.formCreate = this.createFormGroup();
  }

  ngOnInit(): void { }

  searchFormGroup(){
    return new FormGroup({
      user: new FormControl(''),
    });
  }

  createFormGroup(){

    return new FormGroup({

      id: new FormControl(),

      nombre: new FormControl('', [
        Validators.required
      ]),

      rol: new FormControl('', [
        Validators.required
      ]),

      activo: new FormControl(),
      noActivo: new FormControl(),

    });

  }

  search(){
    this.spinner = true;
    this.tableInfoUser = false;
    this.editActive = false;
    this.deleteActive = false;
    this.formCreate.reset();

    if (this.formSearch.value.user == '' || this.formSearch.value.user == undefined) {
    
      this.nameUser = 'all';
    
    } else {

      this.nameUser = this.formSearch.value.user;
    }

    this.usersService.getUsersByName(this.nameUser).subscribe(
      users => {

        if ( users.success ) {
          
          this.users = users.response;
          this.spinner = false;

        } else if (!users.success) {
            
          this.spinner = false;
          this.toastr.info(users.response, 'Exito!');
        }

      },
      error => {
        this.users = [];
        this.spinner = false;
        this.toastr.info(error.error.response, 'Exito!');
      }
    );
    
  }

  create(){
    this.tableInfoUser = true;
    this.saveActive = true;
    this.updateActive = false;
    this.editActive = false;
    this.deleteActive = false;
    this.editFieldsActive = true;
    
    this.formCreate.reset();

    this.rolsService.getRols().subscribe(
      rols => {

        if ( rols.success ) {
          this.rols = rols.response;
        } 

      },
      error => {
        this.rols = [];
        this.toastr.error(error.error.response, 'Error');
      }
    );    
  }

  save(){
    this.spinner = true;
    
    if (this.formCreate.valid) {

      this.usersService.getUsersByName(this.formCreate.value.nombre).subscribe(
        users => {

          if ( users.success ) {

            this.userExist = true;

          } else if (!users.success) {
            
            this.userExist = false;
          }
  
        },
        error => {
          this.userExist = false;
        }
      );

      setTimeout(() => {

        if (!this.userExist) {
  
          let data = {
            nombre: this.formCreate.value.nombre,
            rol: this.formCreate.value.rol,
            activo: 0,
          };
    
          if (this.formCreate.value.activo) {
            data.activo = 1;
          } else if (this.formCreate.value.noActivo) {
            data.activo = 0;
          }
    
          this.usersService.createUser(data).subscribe(
            data => {
              this.spinner = false;
              this.toastr.success(data.response, 'Exito!');
              this.clearForm();
              this.search();
            },
            error => {
              this.spinner = false;
              this.toastr.error(error.error.response, 'Error');
            }
          );
        
        } else {

          this.spinner = false;
          this.toastr.warning('El nombre ' + this.formCreate.value.nombre + ' ya existe!', 'Advertencia');        
        }

      }, 3000);

    } else {

      this.spinner = false;
      alert('No se puede enviar el formulario, algunos campos están vacíos');
    }    
  }

  update(){
    this.spinner = true;
    let exitsName = false;

    this.nameTemporal = this.nameTemporal.toLowerCase();
    this.formCreate.value.nombre = this.formCreate.value.nombre.toLowerCase();
    
    if (this.formCreate.valid) {

      if (this.nameTemporal != this.formCreate.value.nombre) {
        exitsName = this.verifyNameUpdate(this.formCreate.value.nombre);
      }

      if (!exitsName) {

        let data = {
          id: this.formCreate.value.id,
          nombre: this.formCreate.value.nombre,
          rol: this.formCreate.value.rol,
          activo: 0,
        };
  
        if (this.formCreate.value.activo) {
          data.activo = 1;
        } else if (this.formCreate.value.noActivo) {
          data.activo = 0;
        }
  
        this.usersService.updateUser(data.id, data).subscribe(
          data => {
            this.spinner = false;
            this.toastr.success(data.response, 'Exito!');
            this.clearForm();
            this.search();
          },
          error => {
            this.spinner = false;
            this.toastr.error(error.error.response, 'Error');
          }
        );

      } else {
        this.spinner = false;
        this.toastr.warning('El nombre ' + this.formCreate.value.nombre + ' ya existe!', 'Advertencia');
      }
      

    } else {

      alert('No se puede enviar el formulario, algunos campos están vacíos');
    }    
  }

  edit(){
    this.saveActive = false;
    this.updateActive = true;
    this.editActive = false;
    this.editFieldsActive = true;
  }

  delete(){
    let id = this.formCreate.value.id;

    const dialogRef = this.dialog.open(DialogDeleteComponent, {
      width: '300px',
      data: {id: id}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.confirmDelete(result, id);
    });
  }

  confirmDelete(deleteUser, id){
    this.spinner = true;

    if (deleteUser) {

      this.usersService.deleteUser(id).subscribe(
        data => {
          this.spinner = false;
          this.toastr.success(data.response, 'Exito!');
          this.clearForm();
          this.search();
        },
        error => {
          this.spinner = false;
          this.toastr.error(error.error.response, 'Error');
        }
      );

    } else  {

      this.spinner = false;
    }
  } 

  select(user){
    let checkActive = 0;
    let checkNoActive = 0;
    this.selectedValue = user.rol.id;
    this.nameTemporal = user.nombre;

    if (user.activo == 1) {
      checkActive = 1;
    } else if (user.activo == 0){
      checkNoActive = 1;
    }

    this.create();

    this.tableInfoUser = true;
    this.saveActive = false;
    this.editActive = true;
    this.deleteActive = true;
    this.editFieldsActive = false;

    this.formCreate.patchValue({
      id: user.id,
      nombre: user.nombre,
      rol: user.rol.id,
      activo: checkActive,
      noActivo: checkNoActive
    }); 
    
  }

  selectCheckActivo(){
    let checkActive = this.formCreate.value.activo;
    let checkNoActive = 0;

    if (!checkActive) {
      checkNoActive = 1;
    }
    
    this.formCreate.patchValue({
      noActivo: checkNoActive,
    }); 
  }

  selectCheckNoActivo(){
    let checkNoActive = this.formCreate.value.noActivo;
    let checkActive = 0;

    if (!checkNoActive) {
      checkActive = 1;
    }
    
    this.formCreate.patchValue({
      activo: checkActive,
    }); 
  }

  verifyNameUpdate(name): any {

    let existe = false;

    this.users.forEach( function(valor, indice, array) {
      if (name.toLowerCase() == valor.nombre.toLowerCase()) {
        existe = true;
      }
    });

    return existe;
  }
  
  clearForm(){
    this.tableInfoUser = false;
    this.formSearch.reset();
    this.formCreate.reset();
  }

  get nombre(): any { return this.formCreate.get('nombre'); }
  get rol(): any { return this.formCreate.get('rol'); }
  get activo(): any { return this.formCreate.get('activo'); }
  get noActivo(): any { return this.formCreate.get('noActivo'); }
}
